#!/usr/bin/env bash
cd $(dirname $0)
APP_DIR=$HOME/webapps/$2

if [ -z "$1" ] || [ -z "$2"]; then
	echo "Usage: permission.sh <target-user> <target-app>"
	exit 1
fi

if [[ ! -d "$APP_DIR" ]]; then
	echo "Application directory does not exist!"
	exit 1
fi

if [[ "$1" == "$(whoami)" ]]; then
	echo "You cannot lock yourself down!"
	exit 1
fi

read -p "Confirm that you will be restricting user [$1] to [$APP_DIR]? "

setfacl -m u:$1:--- $HOME/webapps/*
setfacl -m u:$1:--x $HOME

if [[ -d $HOME/webapps/$2 ]]
  then
    setfacl -R -m u:$1:rwx $APP_DIR
    setfacl -R -m d:u:$1:rwx $APP_DIR
    setfacl -R -m d:u:shsu:rwx $APP_DIR
fi
echo "Done!"
